import React,{Component} from 'react';
import {List, ListItem} from 'react-mdl';
class Aboutme extends Component {
  render(){
    return(
      <div>
        <h1>Know More About Me</h1>
        <List>
          <ListItem>DOB :- 01-03-1989</ListItem>
          <ListItem>Address :- Deoghara Chandra Tola, Amarpur, Lakhisarai, Pin No :- 811106, Bihar</ListItem>
          <ListItem>Linguistic Proficiency :- English, Hindi</ListItem>
          <ListItem>Sex:- Male</ListItem>
          <ListItem>Nationality:- Indian</ListItem>
          <ListItem>Hobbies:- Net Surfing, Playing cricket</ListItem>
        </List>
      </div>
    )
  }
}


export default Aboutme;
