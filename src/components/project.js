import React,{Component} from 'react';
import {List, ListItem} from 'react-mdl';
class Project extends Component {
  render(){
    return(
      <div>
        <h1>This is my Project Details</h1>
        <List>
          <ListItem>Automatic fruit sorting Robot</ListItem>
          <ListItem>Exploiting vulnerabilities in Embedded User Interfaces in Android</ListItem>
          <ListItem>Ubiquitous Clicker application for Heterogeneous Devices in a Distributed Environment</ListItem>
        </List>
      </div>
    )
  }
}


export default Project;
