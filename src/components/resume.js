import React,{Component} from 'react';
import {List, ListItem} from 'react-mdl';


class Resume extends Component {
  render(){
    return(
      <div>
        <h1>My Work Details</h1>
        <List>
          <ListItem>Ubiquitous Clicker application for Heterogeneous Devices in a Distributed Environment</ListItem>
          <ListItem>Clicker system optimization</ListItem>
          <ListItem>Web Technologies: HTML5, CSS, Bootstrap, JavaScript and React</ListItem>
        </List>


      </div>
    )
  }
}


export default Resume;
