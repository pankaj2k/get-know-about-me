import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Lending from './lendingpage';
import Project from './project';
import Contacts from './contacts';
import Resume from './resume';
import Aboutme from './aboutme';


const Main = () => (
  <Switch>
  <Route exact path="/" component={Lending} />
  <Route path="/project" component={Project} />
  <Route path="/contacts" component={Contacts} />
  <Route path="/resume" component={Resume} />
  <Route path="/aboutme" component={Aboutme} />
  </Switch>
)

export default Main;
